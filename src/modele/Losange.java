package modele;

import java.awt.Polygon;

/**
 * Classe qui d�finie un Losange.
 * 
 * @author gandar8u guillard7u
 *
 */
public class Losange extends Quadrilatere{	
	/**
	 * Ce constructeur instancie un Losange al�atoire.
	 */
	public Losange(){
		super();
		tab_mem = new Point[2];
		int x;
		int y;

		for(int i = 0; i<2; i++)
		{
			x = (int)(Math.random() * 100);  
			y = (int)(Math.random() * 100);
			tab_mem[i]= new Point(x, y);
		}	

		modifierPoints(tab_mem);
	}
	
	/**
	 * Cette m�thode retourne en r�sultat le nombre de points dont on a besoin pour la saisie d'un Losange.
	 * 
	 * @return nombre de clic
	 */
	public int nbClics(){
		return 2;
	}
	
	/**
	 * Cette methode modifie le Losange conformement a un ensemble de deux points de saisie.
	 * 
	 * @param tab_saisie - tableau contenant les nouveaux points de saisie.
	 */
	public void modifierPoints(Point[] tab_saisie){
		Point a = tab_saisie[0]; // Coin haut gauche.
		Point b = tab_saisie[1]; // Coin bas droit.
		tab_mem[0] = a;
		tab_mem[1] = b;
		
		Point c = null;
		Point d = null;
		
		if (b.rendreY()>a.rendreX())
		{
			d = new Point(b.rendreX(), b.rendreY());
			b = new Point(d.rendreX(), a.rendreY() - (d.rendreY() - a.rendreY()) );
			c = new Point( b.rendreX() + (b.rendreX() - a.rendreX()) , a.rendreY());
		}
		else
		{
			d = new Point(b.rendreX(), a.rendreY() + (a.rendreY() - b.rendreY()) );
			c = new Point( b.rendreX() + (b.rendreX() - a.rendreX()) , a.rendreY());
		}
		
		Point[] points = new Point[4];
		points[0]=a;
		points[1]=b;
		points[2]=c;
		points[3]=d;
		
		int xPoints[] = new int[4];
		int yPoints[] = new int[4];
		
		for (int i = 0; i < 4; i++)
		{
			xPoints[i] = points[i].rendreX();
			yPoints[i] = points[i].rendreY();
		}
		
		p = new Polygon (xPoints, yPoints, 4);
	}
}
	
