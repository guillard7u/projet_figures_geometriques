package modele;

import java.awt.*;

/**
 * Cette classe abstraite est la super classe de tous les polygones.
 * 
 * @author gandar8u guillard7
 *
 */
public abstract class Polygone extends FigureColoree{
	/**
	 * Polygone de l'API Java (classe Polygon).
	 */
	protected Polygon p;
	
	/**
	 * Constructeur vide.
	 */
	public Polygone()
	{
		super();
		p = new Polygon();
	}
	
	/**
	 * M�thode affichant un polygone (fait appel � fillPolygon de la classe Java Polygon).
	 * 
	 * @param g - contexte graphique.
	 */
	public void affiche(Graphics g)
	{
		super.affiche(g);		
		g.setColor(couleur);		
		g.fillPolygon(p);
	}	
	
	/**
	 * Cette m�thode modifie le polygone conform�ment � un ensemble de points de saisie (ses nouveaux sommets).
	 * 
	 * @param tab_saisie - tableau contenant les nouveaux points de saisie.
	 */
	public void modifierPoints(Point[] tab_saisie)
	{
		int taille = tab_saisie.length;
		int xPoints[] = new int[taille];
		int yPoints[] = new int[taille];
		
		for (int i = 0; i < taille; i++)
		{
			int x = tab_saisie[i].rendreX();
			int y = tab_saisie[i].rendreY();
			
			tab_mem[i]= new Point(x, y);
			xPoints[i] = x;			
			yPoints[i] = y;
		}		
		p = new Polygon(xPoints, yPoints, taille);
	}
	
	/**
	 * M�thode qui retourne en r�sultat le nombre de points dont on a besoin,
	 * en g�n�ral, pour la saisie d'un polygone.
	 * 
	 * @return nc nombre de clics.
	 */
	public int nbClics(){
		return tab_mem.length;
	}


	/**
	 * M�thode qui permet de modifier les points de m�morisation � partir de points de saisie.
	 */
	@Override
	public boolean estDedans(int x, int y) {
		return p.contains(x, y);
	}
}