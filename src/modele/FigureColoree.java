package modele;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Classe FigureColoree.
 * 
 * @author gandar8u guillard7u
 *
 */

public abstract class FigureColoree {
	/**
	 * Taille du carre de s�lection.
	 */
	protected static final int TAILLE_CARRE_SELECTION = 10; // Pass� en protected pour Cercle.

	/**
	 * P�ripherie du carr� de s�lection.
	 */
	private static final int PERIPHERIE_CARRE_SELECTION = 5;

	/**
	 * Tableau des points d'une figure.
	 */
	protected Point[] tab_mem;
	
	/**
	 * Attribut qui indique si la figure est selectionn�e.
	 */
	protected boolean selected;  // Pass� en protected pour Cercle.
	/**
	 * Couleur de la figure.
	 */
	protected Color couleur;

	/**
	 * Constructeur de la classe FigureColoree.
	 */
	public FigureColoree(){
		couleur = Color.black;
		selected = false;
		int taille = this.nbClics();
		tab_mem = new Point[taille];
	}

	/**
	 * M�thode abstraite qui retourne le nombre de points de m�morisation . 
	 */
	public abstract int nbPoints();

	/**
	 * M�thode abstraite qui retourne le nombre de points de saisie (nombre de clics). 
	 */
	public abstract int nbClics();

	/**
	 * Cette m�thode retourne vrai si le point dont les coordonnees sont pass�es en parametre se trouve a l'int�rieur de la figure. 
	 * 
	 * @param x - abscisse.
	 * @param y - ordonn�e.
	 */
	public abstract boolean estDedans(int x, int y);

	/**
	 * M�thode abstraite qui permet de modifier les points de m�morisation � partir de points de saisie. 
	 * 
	 * @param ps - tableau de Point.
	 */
	public abstract void modifierPoints(Point[] ps);

	/**
	 * M�thode d'affichage de la figure.
	 * Si la figure est s�lectionn�e des petits carr�s sont dessin�s autour des points de m�morisation.
	 * 
	 * @param g - environnement graphique.
	 */
	public void affiche(Graphics g){
		if(selected)
		{
			g.setColor(Color.gray);
			for(int i=0;i<tab_mem.length;i++){ 
				g.drawRect(tab_mem[i].rendreX()-TAILLE_CARRE_SELECTION/2,tab_mem[i].rendreY()-TAILLE_CARRE_SELECTION/2
						,TAILLE_CARRE_SELECTION,TAILLE_CARRE_SELECTION);
			}
		}
	}

	/**
	 * Cette M�thode permet d'effectuer une translation des coordonn�es des points de m�morisation de la figure. 
	 * 
	 * @param dx - d�placement sur l'axe des abscisses.
	 * @param dy - d�placement sur l'axe des ordonn�es.
	 */
	public void translation(int dx, int dy){
		for(int i=0;i<tab_mem.length;i++){
			tab_mem[i].translation(dx, dy);
			modifierPoints(tab_mem);
		}
	}

	/**
	 * Cette M�thode permet d'effectuer une transformation des coordonn�es des points de m�morisation de la figure. 
	 * 
	 * @param dx - d�placement sur l'axe des abscisses.
	 * @param dy - d�placement sur l'axe des ordonn�es.
	 * 
	 * @param indice - indice dans tab_mem du point a modifier.
	 */
	public void transformation(int dx, int dy, int indice){
		tab_mem[indice].translation(dx, dy);
		modifierPoints(tab_mem);
	}

	/**
	 * M�thode qui d�tecte un point se trouvant pr�s d'un carre de s�lection. 
	 * 
	 * @param x - abscisse d'un clic de souris.
	 * @param y - ordonnee d'un clic de souris. 
	 * 
	 * @return i - l'indice dans tab_mem du point se trouvant pr�s d'un carre de s�lection.
	 */
	public int carreDeSelection(int x, int y){
		int i = 0;
		boolean trouve = false;
		while((i<tab_mem.length)&&(trouve == false)) {
			int d = (int) Math.sqrt((tab_mem[i].rendreY() - y)*(tab_mem[i].rendreY() - y) + (tab_mem[i].rendreX() - x)*(tab_mem[i].rendreX() - x));
			System.out.println("Distance : " + d);
			if(d <= PERIPHERIE_CARRE_SELECTION){
				trouve = true;
			}else{
				i++;
			}
		}
		if(trouve){
			return i;
		}else{
			return -1;
		}
	}

	/**
	 * Cette M�thode s�lectionne la figure.
	 */
	public void selectionne(){
		selected = true;
	}

	/**
	 * Cette M�thode d�selectionne la figure.
	 */
	public void deSelectionne(){
		selected = false;
	}

	/**
	 * M�thode permettant de changer la couleur de la figure. 
	 * 
	 * @param c - nouvelle couleur.
	 */
	public void changeCouleur(Color c){
		couleur = c;
	}
}
