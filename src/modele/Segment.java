package modele;

import java.awt.Color;

/**
 *  Classe Segment qui permet de d�finir un segement lors d'un dessin � main lev�.
 * 
 * @author guillard7u gandar8u
 *
 */
public class Segment {
	/**
	 * Coordonn�es du segment.
	 */
	private int x1,x2,y1,y2;
	
	/**
	 * Couleur du segment.
	 */
	private Color couleur;
	
	public Segment(int px1,int py1,int px2,int py2){
		couleur = Color.black;
		x1=px1;
		x2=px2;
		y1=py1;
		y2=py2;
	}

	// Ensemble des getteurs et setteurs.
	public int getX1() {
		return x1;
	}

	public void setX1(int x1) {
		this.x1 = x1;
	}

	public int getX2() {
		return x2;
	}

	public void setX2(int x2) {
		this.x2 = x2;
	}

	public int getY1() {
		return y1;
	}

	public void setY1(int y1) {
		this.y1 = y1;
	}

	public int getY2() {
		return y2;
	}

	public void setY2(int y2) {
		this.y2 = y2;
	}
	
	/**
	 * M�thode d'acc�s � l'attribut priv� couleur.
	 * Utilis� lors de l'affichage d'un segment.
	 * 
	 * @return couleur du segment.
	 */
	public Color getCouleur()
	{
		return couleur;
	}
	
	/**
	 * M�thode qui permet de changer la couleur d'un segment.
	 * 
	 * @param c - couleur que l'on souhaite attribuer � ce segment.
	 */
	public void setCouleur(Color c)
	{
		couleur = c;
	}
	
}
