package modele;

import java.awt.Polygon;

/**
 * Classe d�finissant un Carre.
 * 
 * @author  gandar8u guillard7u
 *
 */
public class Carre extends Rectangle{
	/**
	 * Constructeur de la classe carr� faisant appel au constructeur de Rectangle.
	 */
	public Carre()
	{
		super();
	}

	/**
	 * Cette methode modifie le Carre conform�ment a un ensemble de deux points de saisie.
	 * Le c�t� d'un Carre est obtenu par la distance horizontale entre le premier et le deuxi�me clic.
	 * 
	 * @param tab_saisie - tableau contenant les nouveaux points de saisie.
	 */
	public void modifierPoints(Point[] tab_saisie){
		Point a = tab_saisie[0]; // Coin haut gauche.
		Point b = tab_saisie[1]; // Coin bas droit.

		tab_mem[0] = a;
		tab_mem[1] = b;

		Point c = new Point(b.rendreX(), a.rendreY()); // Coin haut droit.
		Point d = null; // Coin bas gauche.
		b.modifierY(c.rendreY() + (c.rendreX() - a.rendreX()) );
		d = new Point(a.rendreX(), b.rendreY());

		System.out.println("b : \n"+b.rendreX()+" "+b.rendreY());
		System.out.println("d : \n"+d.rendreX()+" "+d.rendreY());
		Point[] points = new Point[4];
		points[0] = a;
		points[1] = c;
		points[2] = b;
		points[3] = d;

		int xPoints[] = new int[4];
		int yPoints[] = new int[4];

		for (int i = 0; i < 4; i++)
		{
			xPoints[i] = points[i].rendreX();
			yPoints[i] = points[i].rendreY();
		}
		p = new Polygon(xPoints, yPoints, 4);
	}


	/**
	 * Cette methode permet d'effectuer une transformation des coordonnees des points de memorisation du carr�.
	 * 
	 * @param dx - deplacement sur l'axe des abscisses.
	 * @param dy - deplacement sur l'axe des ordonnees.
	 * 
	 * @param indice - indice dans tab_mem du point a modifier.
	 */
	public void transformation(int dx, int dy, int indice){
		if (indice == 0)
			tab_mem[indice].translation(dx, dx);
		else
			tab_mem[indice].translation(dx, dy);
		modifierPoints(tab_mem);
	}
}
