package modele;

/**
 * Classe Triangle, classe fille de Polygone.
 * 
 * @author gandar8u guillard7u.
 *
 */
public class Triangle extends Polygone{
	
	/**
	 * Ce constructeur instancie un triangle avec des sommets al�atoires (via le constructeur de Polygone).
	 */
	public Triangle(){
		super();
		Point [] points = new Point[3];
		int x;
		int y;
		for(int i = 0; i<3; i++)
		{
			x = (int)(Math.random() * 100);   
			y = (int)(Math.random() * 100);
			points[i]= new Point(x, y);
		}
		modifierPoints(points);
	}
	
	/**
	 * Cette m�thode retourne en r�sultat le nombre de points de m�morisation d'un triangle.
	 * 
	 * @return nombre de points.
	 */
	public int nbPoints(){
		return 3;
	}
	
		
	/**
	 * M�thode qui retourne en r�sultat le nombre de points dont on a besoin
	 * pour la saisie d'un triangle.
	 * 
	 * @return nombre de clics.
	 */
	public int nbClics(){
		return 3;
	}
}
