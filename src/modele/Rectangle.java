package modele;

import java.awt.Polygon;

/**
 * Classe Rectangle
 * 
 * @author guillard7u gandar8u
 *
 */

public class Rectangle extends Quadrilatere{
	
	/**
	 * Ce constructeur instancie un rectangle aleatoire.
	 */
	public Rectangle(){
		super();
		tab_mem = new Point[2];
		int x;
		int y;
		for(int i = 0; i<2; i++)
		{
			x = (int)(Math.random() * 100);  
			y = (int)(Math.random() * 100);
			tab_mem[i]= new Point(x, y);
		}	
		modifierPoints(tab_mem);
	}
	
	/**
	 * Cette m�thode retourne en r�sultat le nombre de points dont on a besoin pour la saisie d'un rectangle.
	 * 
	 * @return nombre de clic
	 */
	public int nbClics(){
		return 2;
	}
	
	/**
	 * Cette m�thode modifie le rectangle conformement a un ensemble de deux points de saisie.
	 * Ces deux points forment une diagonale du rectangle 
	 * (du coin superieur gauche vers le coin inferieur droit).
	 * 
	 * @param tab_saisie - tableau contenant les nouveaux points de saisie.
	 */
	public void modifierPoints(Point[] tab_saisie){
		Point a = tab_saisie[0]; // Coin haut gauche.
		Point b = tab_saisie[1]; // Coin bas droit.
		tab_mem[0] = a;
		tab_mem[1] = b;
		
		Point c = new modele.Point (b.rendreX(),a.rendreY()); // Coin haut droit.
		Point d = new modele.Point (a.rendreX(),b.rendreY()); // Coin haut gauche.
		
		Point[] points = new Point[4];
		points[0]=a;
		points[1]=c;
		points[2]=b;
		points[3]=d;
		
		int xPoints[] = new int[4];
		int yPoints[] = new int[4];
		
		for (int i = 0; i < 4; i++)
		{
			xPoints[i] = points[i].rendreX();
			yPoints[i] = points[i].rendreY();
		}
		
		p = new Polygon (xPoints, yPoints, 4);
	}	
}
