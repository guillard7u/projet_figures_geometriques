package modele;

/**
 * Classe Quadrilatere
 * 
 * @author gandar8u guillard7u
 *
 */
public class Quadrilatere extends Polygone{

	/**
	 * Ce constructeur instancie un quadrilat�re avec des sommets al�atoires 
	 * (via le constructeur de Polygone).
	 */
	public Quadrilatere() 
	{
		super();
		tab_mem = new Point[4];
		int x;
		int y;
		for(int i = 0; i<4; i++)
		{
			x = (int)(Math.random() * 100);
			y = (int)(Math.random() * 100);
			tab_mem[i]= new Point(x, y);
		}
		modifierPoints(tab_mem);
	}
	
	/**
	 * M�thode qui retourne en r�sultat le nombre de points de m�morisation.
	 */
	public int nbPoints() 
	{
		int ret = 0;
		for(int i = 0; i< tab_mem.length; i++)
		{
			if(tab_mem[i]!= null)
			{
				ret ++;
			}
		}
		return ret;
	}	
	
	/**
	 * M�thode qui retourne en r�sultat le nombre de points dont on a besoin
	 * pour la saisie d'un Quadrilatere.
	 * 
	 * @return nc nombre de clics.
	 */
	public int nbClics(){
		return 4;
	}
}
