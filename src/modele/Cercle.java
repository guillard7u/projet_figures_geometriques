package modele;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Classe d�finissant un Cercle.
 * 
 * @author gandar8u guillard7u
 *
 */
public class Cercle extends FigArrondie{

	/**
	 * Ce constructeur instancie un cercle al�atoire.
	 */
	public Cercle(){
		super();
		tab_mem = new Point[2];
		int x;
		int y;
		for(int i = 0; i<2; i++)
		{
			x = (int)(Math.random() * 100);  
			y = (int)(Math.random() * 100);
			tab_mem[i]= new Point(x, y);
		}
	}

	/**
	 * M�thode affichant une FigArrondie (fait appel � fillOval).
	 * 
	 * @param g - contexte graphique.
	 */
	public void affiche(Graphics g)
	{
		int x1;
		int y1;
		int x2;
		int y2;
		g.setColor(couleur);

		int xCentre = 0;
		int yCentre = 0;

		x1 = tab_mem[0].rendreX();
		x2 = tab_mem[1].rendreX();
		y1 = tab_mem[0].rendreY();
		y2 = tab_mem[1].rendreY();

		int rayon = (int)Math.sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) );
		// Un cercle cr�e par fillOval est d�limit� par un rectangle, d'o� le besoin de red�finir le centre du cercle.
		xCentre = tab_mem[0].rendreX()- rayon;
		yCentre = tab_mem[0].rendreY()- rayon;

		g.fillOval(xCentre, yCentre, rayon*2, rayon*2);	
		
		// Dessin des carr�s de s�lection.
		if(selected)
		{
			g.setColor(Color.gray);
			System.out.println("Affiche");
			g.drawRect(tab_mem[1].rendreX()-TAILLE_CARRE_SELECTION/2,tab_mem[1].rendreY()-TAILLE_CARRE_SELECTION/2
						,TAILLE_CARRE_SELECTION,TAILLE_CARRE_SELECTION);
			
			if(couleur == Color.black)
				g.setColor(Color.white);
			
			x1 = tab_mem[0].rendreX() - TAILLE_CARRE_SELECTION/2;
			y1 = tab_mem[0].rendreY();
			x2 = tab_mem[0].rendreX() + TAILLE_CARRE_SELECTION/2;
			y2 = tab_mem[0].rendreY();
			g.drawLine(x1, y1, x2, y2);
			
			x1 = tab_mem[0].rendreX();
			y1 = tab_mem[0].rendreY() - TAILLE_CARRE_SELECTION/2;
			x2 = tab_mem[0].rendreX();
			y2 = tab_mem[0].rendreY() + TAILLE_CARRE_SELECTION/2;
			g.drawLine(x1, y1, x2, y2);			
		}
	}


	/**
	 * Cette m�thode modifie le Cercle conform�ment � un ensemble de deux points de saisie.
	 * 
	 * @param tab_saisie - tableau contenant les nouveaux points de saisie.
	 */
	public void modifierPoints(Point[] tab_saisie)
	{
		tab_mem = tab_saisie;		
	}
	
	public boolean estDedans(int x, int y)
	{
		int x1 = tab_mem[0].rendreX();
		int x2 = tab_mem[1].rendreX();
		int y1 = tab_mem[0].rendreY();
		int y2 = tab_mem[1].rendreY();

		int rayon = (int)Math.sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) );
		int distance = (int)Math.sqrt( (x-x1)*(x-x1) + (y-y1)*(y-y1) );
		
		return distance <= rayon;
	}


	/**
	 * Cette m�thode retourne en r�sultat le nombre de points dont on a besoin pour la saisie d'un cercle.
	 * 
	 * @return nombre de clic
	 */
	public int nbClics(){
		return 2;
	}

	/**
	 * M�thode qui retourne en r�sultat le nombre de points de m�morisation.
	 */
	public int nbPoints() // TODO
	{
		int ret = 0;
		for(int i = 0; i< tab_mem.length; i++)
		{
			System.out.println(tab_mem[1]);
			if(tab_mem[i]!= null)
			{
				ret ++;
			}
		}
		return ret;
	}
}
