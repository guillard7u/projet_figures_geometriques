package modele;

import java.awt.Graphics;

/**
 * Cette classe abstraite est la super classe de toutes les Figures Arrondies.
 * 
 * @author gandar8u guillard7u
 *
 */
public abstract class FigArrondie extends FigureColoree{
	
	/**
	 * Constructeur vide.
	 */
	public FigArrondie()
	{
		super();
	}
	
	/**
	 * M�thode abstraite affichant une FigArrondie.
	 * 
	 * @param g - contexte graphique.
	 */
	public abstract void affiche(Graphics g);	
	
	/**
	 * M�thode qui retourne en r�sultat le nombre de points dont on a besoin,
	 * en g�n�ral, pour la saisie d'un polygone.
	 * 
	 * @return nc nombre de clics n�cessaires � la construction de la figure.
	 */
	public int nbClics(){
		return tab_mem.length; //         /!\ TODO V�rifier si correct, ou m�thode suppl�mentaire dans Quadrilat�re.
	}

	/**
	 * M�thode qui renvoie si le point doint les coordonn�es sont en param�tre de la m�thode
	 * se trouve dans la figure.
	 */
	public abstract boolean estDedans(int x, int y);
}
