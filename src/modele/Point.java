package modele;

/**
 * Classe Point
 * 
 * @author gandar8u guillard7u
 *
 */
public class Point {
	/**
	 * Abscisse du point.
	 */
	private int x;
	
	/**
	 * Ordonn�e du point.
	 */
	private int y;
	
	/**
	 * Constructeur de la classe Point.
	 * @param abs - abscisse du point � cr�er.
	 * @param ord - ordonn�e du point � cr�er.
	 */
	public Point(int abs, int ord)
	{
		x = abs;
		y = ord;
	}
	
	/**
	 * M�thode calculant la distance entre deux points.
	 * 
	 * @param p2 - deuxi�me point.
	 * @return la distance sous forme de double.
	 */
	public double distance(Point p2)
	{
		return Math.sqrt( (p2.rendreX() - x)*(p2.rendreX() - x)  +  (p2.rendreY() - y)*(p2.rendreY() - y));
	}
	
	/**
	 * M�thode renvoyant l'abscisse du point.
	 * @return
	 */
	public int rendreX()
	{
		return x;
	}
	
	/**
	 * M�thode renvoyant l'ordonn�e du point.
	 * @return
	 */
	public int rendreY()
	{
		return y;
	}
	
	/**
	 * M�thode incr�mentant l'abscisse du point.
	 * 
	 * @param incx - incr�ment � appliquer.
	 */
	public void incrementerX(int incx)
	{
		x += incx;
	}
	
	/**
	 * M�thode incr�mentant l'ordonn�e du point.
	 * @param incy - incr�ment de y.
	 */
	public void incrementerY(int incy)
	{
		y += incy;
	}
	
	/**
	 * M�thode modifiant l'abscisse du point.
	 * 
	 * @param x - nouvelle abscisse du point.
	 */
	public void modifierX(int x)
	{
		this.x = x;
	}
	
	/**
	 * M�thode modifiant l'ordonn�e du point.
	 * 
	 * @param y - nouvelle ordonn�e du point.
	 */
	public void modifierY(int y)
	{
		this.y = y;
	}
	
	/**
	 * M�thode translatant le point.
	 * 
	 * @param dx - deplacement en abscisse.
	 * @param dy - deplacement en ordonn�e.
	 */
	public void translation(int dx, int dy)
	{
		incrementerX(dx);
		incrementerY(dy);
	}
}
