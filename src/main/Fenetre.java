package main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Scanner;

import javax.swing.JFrame;

import vue.DessinFigures;
import vue.PanneauChoix;

/**
 * Cette classe d�finit l'interface utilisateur et la m�thode main.
 * 
 * @author gandar8u guillard7u
 *
 */
public class Fenetre extends JFrame{
	
	/**
	 * DessinFigures
	 */
	private DessinFigures dessin;
	
	/**
	 * Constructeur de la classe.
	 * 
	 * @param s - objet de type String (Nom de la fenetre).
	 * @param w - objet de type int (largeur de la fenetre).
	 * @param h - objet de type int (hauteur de la fenetre).
	 */
	public Fenetre(String s, int w, int h){
		this.setTitle(s);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		dessin = new DessinFigures();
		PanneauChoix pc = new PanneauChoix(dessin);
		this.add(pc , BorderLayout.NORTH);		
		this.add(dessin, BorderLayout.CENTER);
		this.setPreferredSize(new Dimension(w,h));
		this.pack();
		this.setVisible(true);
		this.requestFocusInWindow();
	}
	
	/**
	 * Principale de la classe.
	 * 
	 * Cree une Fenetre avec des parametres rentrees au clavie.
	 * 
	 * @param args
	 */
	public static void main (String[] args){
		// Si l'utilisateur souhaite modifier les param�tres du JFrame.
		/*
		Scanner sc = new Scanner(System.in);	
		System.out.println("entrer nom : ");
		String entreNom = sc.nextLine();
		System.out.println("entrer largeur : ");
		int entreW = sc.nextInt();
		System.out.println("entrer hauteur : ");
		int entreH = sc.nextInt();
		Fenetre f = new Fenetre(entreNom, entreW, entreH);
		sc.close();
		*/
		
		Fenetre f = new Fenetre("Figures Color�es", 500, 500);
	}
}


