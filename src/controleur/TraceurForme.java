package controleur;

import java.awt.Graphics;
import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

import modele.Segment;
import vue.DessinFigures;

/**
 * Classe TraceurForme pour le trac� � main lev�.
 * 
 * @author gandar8u guillard7u
 *
 */
public class TraceurForme extends MouseInputAdapter{
	/**
	 * Contexte graphique.
	 */
	private Graphics g;
	
	/**
	 * Abscisse d'un clic de souris.
	 */
	private int last_x;
	
	/**
	 * Ordonn�e d'un clic de souris.
	 */
	private int last_y;
	
	/**
	 * Constructeur de la classe.
	 * 
	 * @param gr - contexte graphique.
	 */
	public TraceurForme(Graphics gr)
	{		
		g = gr;
	}
	
	/**
	 * M�thode d�butant le trac� � main lev�e.
	 * 
	 * @param e - �v�nement clic de souris.
	 */
	public void mousePressed(MouseEvent e)
	{
		if(SwingUtilities.isLeftMouseButton(e))
		{
			last_x = e.getX();
			last_y = e.getY();
			System.out.println("MousePressed");
			System.out.println("x = "+last_x);
			System.out.println("y = "+last_y);
		}
	}
	
	/** 
	 * M�thode effectuant le trac� � main lev�e.
	 *  
	 * @param e - �v�nement clic de souris. 
	 */ 
	public void mouseDragged(MouseEvent e)
	{ 
		if(SwingUtilities.isLeftMouseButton(e)) 
		{ 
			int x = e.getX(); 
			int y = e.getY(); 
			
			Segment seg = new Segment(last_x, last_y, x, y); 
			seg.setCouleur(g.getColor());
			
			DessinFigures fg = (DessinFigures)e.getSource(); 
			fg.ajoutSegment(seg); 
			
			last_x = x; 
			last_y = y; 
		} 
	} 
} 
