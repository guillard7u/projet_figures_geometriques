package controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.SwingUtilities;

import modele.FigureColoree;
import modele.Point;
import vue.DessinFigures;

/**
 * Classe impl�mentant la cr�ation de figures g�om�triques via des clics de souris.
 * 
 * @author gandar8u guillard7u
 *
 */
public class FabricantFigures implements MouseListener{
	/**
	 * Figure en cours de fabrication.
	 */
	private FigureColoree figure_en_cours_de_fabrication;
	
	/**
	 * Accumule de nombre de clics de souris.
	 */
	private int nb_points_cliques;
	
	/**
	 * Tableau contenant des points cr��s � partir de clics de souris.
	 */
	private Point[] points_cliques;
	
	/**
	 * Constructeur de la classe.
	 * 
	 * @param fc - figure vide cr��e dans la classe Dialogue.
	 */
	public FabricantFigures(FigureColoree fc)
	{
		figure_en_cours_de_fabrication = fc;
		nb_points_cliques = 0;
		points_cliques = new Point[figure_en_cours_de_fabrication.nbClics()];   //    /!\ pas s�r  TODO
	}

	/**
	 * M�thode de l'interface MouseListener non utilis�e.
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	/**
	 * M�thode de l'interface MouseListener non utilis�e.
	 */
	@Override
	public void mouseExited(MouseEvent e) {
	}
	
	/**
	 * M�thode de l'interface MouseListener non utilis�e.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
	}
	
	/**
	 * M�thode de l'interface MouseListener non utilis�e.
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
	}
	
	/**
	 * M�thode impl�mentant la cr�ation d'une figure g�om�trique via des clics de souris.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if(SwingUtilities.isLeftMouseButton(e))
		{
			int x = e.getX();
			int y = e.getY();
			
			int nbClics = figure_en_cours_de_fabrication.nbClics();
			if(nb_points_cliques < nbClics)
			{
				points_cliques[nb_points_cliques] = new Point(x, y);
				nb_points_cliques++;	
			}
			if(nb_points_cliques == nbClics)
			{
				figure_en_cours_de_fabrication.modifierPoints(points_cliques);
				DessinFigures df = (DessinFigures)(e.getSource());
				df.ajoute(figure_en_cours_de_fabrication);
				figure_en_cours_de_fabrication.deSelectionne();
				df.removeMouseListener(this);				
				nb_points_cliques = 0;
			}
		}
		
		// Permet d'arr�ter la construction de la figure.
		if(SwingUtilities.isRightMouseButton(e))
		{
			DessinFigures df = (DessinFigures)(e.getSource());
			df.removeMouseListener(this);
		}
	}

	

	
	
	

}
