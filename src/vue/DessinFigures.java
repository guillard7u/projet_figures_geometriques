package vue;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;

import controleur.FabricantFigures;
import controleur.TraceurForme;
import modele.FigureColoree;
import modele.Segment;

/**
 * Chaque instance de cette classe est un dessin comportant plusieurs figures
 * color�es visualisables � l'�cran dont une seule est s�lectionn�e.
 * 
 * @author gandar8u guillard7u
 *
 */
public class DessinFigures extends JPanel{
	/**
	 * Tableau pouvant contenir jusqu'� 100 figures � visualiser et manipuler.
	 */
	private FigureColoree[] figures;

	/**
	 * Tableau de segments.
	 */
	private ArrayList<Segment> segment;

	/**
	 * Objet "Listener" pour les manipulations et transformations de figures via la souris.
	 */
	private ManipulateurFormes mf;

	/**
	 * Nombre effectif de figures apparaissant dans ce dessin (et donc cr�es).
	 */
	private int nbf;

	/**
	 * Indice de la figure actuellement s�lectionn�e (-1 si aucune figure n'est s�lectionn�e).
	 */
	private int sel;

	/**
	 * Indice du carr� de selection, utilis� dans la modification de point.
	 */
	private int indiceCarreSelection;

	/**
	 * Constructeur vide : dessin ne contenant aucune figure.
	 */
	public DessinFigures()
	{
		mf = new ManipulateurFormes();
		figures = new FigureColoree[100];
		segment = new ArrayList<Segment>();
		nbf = 0;
		sel = -1;
	}

	/**
	 * M�thode activant les manipulations des formes g�om�triques � la souris.
	 */
	public void activeManipulationsSouris()
	{
		mf.trans = true;
		addMouseListener(mf);
		addMouseMotionListener(mf);
	}

	/**
	 * M�thode d�sactivant les manipulations des formes g�om�triques � la souris.
	 */
	public void desactiveManipulationsSouris()
	{ 
		mf.trans = false;
		this.removeMouseListener(mf);  
		this.removeMouseMotionListener(mf);
	} 

	/** 
	 * M�thode qui permet d'ajouter une nouvelle figure au dessin.
	 * 
	 * @param fc - figure � ajouter au dessin.
	 */
	public void ajoute(FigureColoree fc)
	{
		if(nbf<100)
		{
			figures[nbf] = fc;
			//			sel = nbf;
			nbf++;
		}
		supprimeAuditeurs(); 
		desactiveManipulationsSouris();
		repaint();
	}

	/**
	 * M�thode qui retourne le nombre de figures apparaissant/cr�es dans ce dessin.
	 * 
	 * @return nombre de figures sur le dessin / cr�es.
	 */
	public int nbFigures()
	{
		return nbf;
	}

	/**
	 * M�thode qui retourne la figure actuellement s�lectionn�e.
	 * 
	 * @return figure s�lectionn�e, ou null si aucune figure n'est s�lectionn�e.
	 */
	public FigureColoree figureSelection()
	{
		FigureColoree ret = null;
		if (sel != -1) 
		{
			ret = figures[sel];
		}
		return ret;
	}

	/**
	 * methode ajoutSegment qui ajoute un segment a l'arrayList de segments.
	 * 
	 * @param s - segment a ajouter
	 */
	public void ajoutSegment(Segment s) 
	{
		this.segment.add(s); 
		repaint(); 
	}

	/**
	 * Cette m�thode s�lectionne la prochaine figure dans le tableau des figures.
	 * Parcours cyclique du tableau.
	 */
	public void selectionProchaineFigure()
	{
		if(sel == -1)
		{
			if (nbf != 0)
			{
				sel = 0;
			}
		}
		else if (sel < nbf-1) {
			figures[sel].deSelectionne();
			sel ++;
			figures[sel].selectionne();
		}
		else {
			figures[sel].deSelectionne();
			sel = 0;
			figures[sel].deSelectionne();
		}
	}

	/**
	 * M�thode qui permet d'initier le m�canisme �v�nementiel de fabrication
	 * des figures � la souris (ajout du litener).
	 * 
	 * @param fc - forme � construire point par point � la souris.
	 */
	public void construit(FigureColoree fc)
	{
		// La m�thode construit cr�e un objet de la classe FabricantFigures. 
		// Cette classe impl�mente l'interface MouseListener en lui passant la figure � construire en parem�tre.
		// Elle lie ce "listener" � DessinFigures.

		FabricantFigures ff = new FabricantFigures(fc);

		addMouseListener(ff);
	}

	/**
	 * Cette m�thode permet d'initier le m�canisme �v�nementiel de trac� quelconque � la souris
	 * (d�finition de la couleur du trac� et ajout des listeners).
	 * 
	 * @param c - couleur du trait.
	 */
	public void trace(Color c)
	{
		Graphics graph = getGraphics();
		graph.setColor(c);
		TraceurForme traceur = new TraceurForme(graph);
		addMouseListener(traceur);
		addMouseMotionListener(traceur);
	}


	/**
	 * M�thode qui supprime tous les auditeurs du DessinFigures.
	 */
	public void supprimeAuditeurs()
	{
		if(sel != -1)
			figures[sel].deSelectionne();
		
		MouseMotionListener[] tabmm = this.getListeners(MouseMotionListener.class);
		MouseListener[] tabml = this.getListeners(MouseListener.class);

		for(int i = 0 ; i<tabmm.length; i++){
			this.removeMouseMotionListener(tabmm[i]);
		}
		for(int i = 0 ; i<tabmm.length; i++){
			this.removeMouseListener(tabml[i]);
		}
		repaint(); // Pour que la d�selection de la figure soit bien prise en compte.
	}
	
	/**
	 * M�thode qui permet d'effacer les figures et les traits affich�s.
	 */
	public void effacer()
	{
		segment.clear();
		for (int i = 0; i < nbf; i++)	
		{
			figures[i] = null;
		}
		sel = -1;
		nbf = 0;
		repaint();
	}

	/**
	 * Cette m�thode redessine toutes les figures du dessin.
	 * 
	 * @param g - environnement graphique de dessin.
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g); 
		for (int i=0; i<nbf; i++) 
		{ 
			// G�rer le changement de couleur ici ? 
			figures[i].affiche(g); 
		} 
		
		for(Segment s:segment)
		{
			g.setColor(s.getCouleur());
			g.drawLine(s.getX1(),s.getY1(),s.getX2(),s.getY2());
		}
	}



	// Beginning ManipulateurFormes
	/**
	 * Classe interne pour le d�placement et la transformation des figures g�om�triques.
	 * 
	 * @author guillard7u
	 *
	 */
	private class ManipulateurFormes extends MouseInputAdapter
	{
		/**
		 * Attribut indiquant l'indice du point proche d'un carr� de s�lection.
		 */
		private int indice;

		/**
		 * Abscisse d'un clic de souris.
		 */
		private int last_x;

		/**
		 * Odonn�e d'un clic de souris.
		 */
		private int last_y;

		/**
		 * Attribut indiquant si un d�placement est en cours.
		 */
		private boolean trans;

		/**
		 * Constructeur vide.
		 */
		private ManipulateurFormes()
		{
			trans = false;
		}

		/**
		 * M�thode permettant de s�lectionner la figure g�om�trique � manipuler.
		 * 
		 * @param e - �v�nement clic de souris.
		 */
		public void mousePressed(MouseEvent e)
		{
			last_x = e.getX();
			last_y = e.getY();
			int cpt = nbf-1;
			boolean trouve = false;
			boolean selectionCarre = false;
			if (sel !=-1)
			{
				indiceCarreSelection = figures[sel].carreDeSelection(last_x, last_y);
				selectionCarre = indiceCarreSelection != -1;
			}
			
			while (cpt >= 0 && !trouve && !selectionCarre)    // Selectionne une nouvelle figure si on clique sur une figure.
			{
				if (figures[cpt].estDedans(last_x, last_y)) 
				{
					if(sel != -1)
					figures[sel].deSelectionne();
					figures[cpt].selectionne();
					sel = cpt;
					mf.trans = true;
					trouve = true;

					// Si on a s�lection� un carr�.
					indiceCarreSelection = figures[sel].carreDeSelection(last_x, last_y);
				}
				else
				{
					cpt--;
				}
			}

			if (sel!=-1) //
			{
				indiceCarreSelection = figures[sel].carreDeSelection(last_x, last_y);
				if (indiceCarreSelection == -1){
					if (!figures[sel].estDedans(last_x, last_y))
					{
						figures[sel].deSelectionne();
						mf.trans = false;
						sel = -1;
					}
				}	
			}
			else if (sel == -1)
			{
				cpt = nbf-1;
				trouve = false;
				while (cpt >= 0 && !trouve)
				{
					if (figures[cpt].estDedans(last_x, last_y)) 
					{
						figures[cpt].selectionne();
						sel = cpt;
						mf.trans = true;
						trouve = true;

						// Si on a s�lection� un carr�.
						indiceCarreSelection = figures[sel].carreDeSelection(last_x, last_y);
					}
					else
					{
						cpt--;
					}
				}
			}

			repaint();
		}

		/**
		 * M�thode d�pla�ant ou transformant la figure g�om�trique s�lectionn�e.
		 *
		 * @param e - �v�nement clic de souris.
		 */
		public void mouseDragged(MouseEvent e)
		{
			int x = e.getX();
			int y = e.getY();

			int dx = x - last_x;   
			int dy = y - last_y;

			if(sel!=-1 && mf.trans)
			{
				if(indiceCarreSelection != -1)
				{
					figures[sel].transformation(dx, dy, indiceCarreSelection);
					last_x = x;
					last_y = y;
				}
				else if(sel!=-1 && mf.trans)
				{	
					figureSelection().translation(dx, dy);
					last_x = x;
					last_y = y;
				}
			}
			repaint();
		}
		// End ManipulateurFormes
	}
}
