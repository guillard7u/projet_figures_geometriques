package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import modele.Carre;
import modele.Cercle;
import modele.FigureColoree;
import modele.Losange;
import modele.Quadrilatere;
import modele.Rectangle;
import modele.Triangle;

/**
 * Classe pour la construction de l'interface utilisateur.
 * 
 * @author gandar8u guillard7u
 *
 */
public class PanneauChoix extends JPanel{
	/**
	 * Tableau des couleurs s�lectionables.
	 */
	private String [] nomCouleurs = {"noir", "rouge", "vert", "jaune","bleu"};

	/**
	 * Tableau des Color associ�es aux couleurs du tableau nomCouleurs.
	 * Est tri� dans l'ordre exact de ce dernier.
	 */
	private Color [] listeCouleurs = {Color.black, Color.red, Color.green, Color.yellow, Color.blue };

	/**
	 * Tableau des figures g�om�triques selectionables pour �tre cr�es en cliquant.
	 */
	private String[] listeFigures = {"Quadrilat�re", "Rectangle", "Triangle", "Carr�", "Losange", "Cercle"};

	/**
	 * ArrayList des boutons JRadioUtilis�es.
	 */
	ArrayList<JRadioButton> buttons;
	
	/**
	 * Couleur actuellement s�lectionn�e.
	 */
	private Color couleur;

	/**
	 * Dessin
	 */
	private DessinFigures dessin;

	/**
	 * FigureColoree
	 */
	private FigureColoree fc;

	/**
	 * Constructeur de la classe.
	 * 
	 * @param d - objet de type DessinFigures (JPanel).
	 */
	public PanneauChoix(DessinFigures d)
	{
		couleur = Color.black;
		dessin = d;
		this.setLayout(new BorderLayout());

		// ----- Beginning JComboBox -----
		final JComboBox choixFigures = new JComboBox (listeFigures);


		choixFigures.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {

				MouseListener[] ml = dessin.getListeners(MouseListener.class);
				for(int i = 0; i < ml.length; i++)
				{
					dessin.removeMouseListener(ml[i]);
				}

				switch (choixFigures.getSelectedIndex()) {
				case 0:
					fc = new Quadrilatere();
					fc.changeCouleur(couleur);
					d.construit(fc);

					break;

				case 1:
					fc = new Rectangle();
					fc.changeCouleur(couleur);
					d.construit(fc);
					break;

				case 2:
					fc = new Triangle();
					fc.changeCouleur(couleur);
					d.construit(fc);
					break;

				case 3:
					fc = new Carre();
					fc.changeCouleur(couleur);
					d.construit(fc);
					break;

				case 4:
					fc = new Losange();
					fc.changeCouleur(couleur);
					d.construit(fc);
					break;

				case 5:
					fc = new Cercle();
					fc.changeCouleur(couleur);
					d.construit(fc);
					break;

				default:
					break;
				}	
			}
		});
		

		/* ----- Gestion Couleur -----*/
		// 		----- Beginning JColorChooser -----
		JButton colorButton = new JButton();
		colorButton.setPreferredSize(new Dimension(40,25));
		colorButton.setBackground(couleur);
		colorButton.setOpaque(true);
		colorButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				couleur = JColorChooser.showDialog(null, "", couleur);
				if(buttons.get(1).isSelected())   // Si "Trac� � main lev�" est s�lectionn�.
				{
					dessin.trace(couleur);
				}
				colorButton.setBackground(couleur);
			}
		});				
		// 		----- End JColorChooser -----

		final JComboBox choixCouleurs = new JComboBox ( nomCouleurs);
		choixCouleurs.addActionListener (new ActionListener(){
			public void actionPerformed (ActionEvent e) {
				int index = choixCouleurs.getSelectedIndex();
				couleur = determineCouleur(index);
				colorButton.setBackground(couleur);

				if(buttons.get(1).isSelected())   // Si "Trac� � main lev�" est s�lectionn�.
				{
					dessin.trace(couleur);
				}
			}
		});		
		/* ----- Fin Gestion Couleur ----- */
		
		// ----- Bouton effacer -----
		JButton effacerButton = new JButton ("Effacer");
		effacerButton.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
				dessin.effacer();
			}
		});
		// ----- Fin bouton effacer -----
		

		JPanel zoneComboBox = new JPanel();
		zoneComboBox.add(choixFigures);
		zoneComboBox.add(choixCouleurs);
		zoneComboBox.add(colorButton);
		zoneComboBox.add(effacerButton);

		// ----- End JComboBox -----

		// ----- Beginning JRadioButton -----

		ActionListener radioFigure = new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				dessin.supprimeAuditeurs();
				choixFigures.setEnabled(true);				
			}
		};

		ActionListener radioTrace = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dessin.supprimeAuditeurs();
				choixFigures.setEnabled(false);
				dessin.trace(couleur);
			}
		};

		ActionListener radioManip = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dessin.supprimeAuditeurs();
				choixFigures.setEnabled(false);
				dessin.activeManipulationsSouris();
			}
		};

		JPanel zoneRadio = new JPanel();
		String[] radioLabels = {"Nouvelle figure", "Trac� � main lev�", "Manipulations"};
		buttons = new ArrayList<JRadioButton>();

		ButtonGroup buttonGroup = new ButtonGroup();
		for(int i = 0; i<radioLabels.length; i++)
		{
			switch (i) {
			case 0:
				buttons.add(new JRadioButton(radioLabels[i],true));
				break;
			case 1:
			case 2:
				buttons.add(new JRadioButton(radioLabels[i]));
				break;
			default:
				break;
			}
		}
		for(int i=0; i<buttons.size(); i++)
		{

			JRadioButton b= buttons.get(i);
			buttonGroup.add(b);
			zoneRadio.add(b);
			switch (i) {
			case 0:
				b.addActionListener(radioFigure);
				break;
			case 1:
				b.addActionListener(radioTrace);
				break;
			case 2:
				b.addActionListener(radioManip);
				break;
			default:
				break;
			}

		}
		// ----- End JRadioButton -----

		// Ajouts dans le JPanel
		this.add(zoneRadio, BorderLayout.NORTH);
		this.add(zoneComboBox, BorderLayout.CENTER);

	}

	/**
	 * M�thode d�terminant la couleur � utiliser.
	 * 
	 * @param index - indice repr�sentant la couleur � utiliser.
	 * @return la couleur choisie par l'utilisateur.
	 */
	private Color determineCouleur(int index)
	{
		Color ret = Color.black;
		if (index > 0 && index < listeCouleurs.length)
		{
			ret = listeCouleurs[index];
		}
		return ret;
	}

}

